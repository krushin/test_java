package PageObject;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Base {
	public static  WebDriver dr;
	public static WebElement element; 
	public static Properties prop;
	
	public Base() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream("D:\\Selenium Autiomation\\Selenium Workspace\\testProject\\src\\main\\java\\PageObject\\config.properties");
			prop.load(ip);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	public static void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium Autiomation\\Selenium Workspace\\Selenum\\chromedriver.exe");
		dr = new ChromeDriver();
		
	}
	public static void url() {
		dr.get(prop.getProperty("url"));
		
		
	}
	public static ExtentHtmlReporter report = new ExtentHtmlReporter("./Report/report.html");	  
	public static ExtentReports extent = new ExtentReports();
	public static ExtentTest extentTest;
//		public static void product() {
//			element = dr.findElement(By.xpath("//input[@id ='twotabsearchtextbox']"));
//			element.sendKeys("camara");
//			element.sendKeys(Keys.ENTER);
//			element = dr.findElement(By.xpath("(//span[contains(@class,'a-size-base-plus a-color-base')])[5]"));
//			element.click();
//			element = dr.findElement(By.xpath("//input[@id = 'add-to-cart-button']"));
//			element.click();
//		}
}
