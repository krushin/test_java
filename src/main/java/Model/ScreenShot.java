package Model;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.apache.commons.io.FileUtils;

public class ScreenShot {
	
	
	public static void screenShot1(WebDriver dr)  {
		//WebDriver xyz = takeScreenShot.dr;  another way
		try {
			TakesScreenshot ts = (TakesScreenshot)dr;
			File source  = ts.getScreenshotAs(OutputType.FILE);			
			FileUtils.copyFile(source, new File("D:\\Selenium Autiomation\\Selenium Workspace\\testProject\\src\\main\\java\\Model\\abc.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("not working");
		}
	}
	
}
